<?php

class Blog extends CI_Model
{

    //get all blog
    public function getAll() {
        $this->db->select('GROUP_CONCAT(c.tag) as tag,b.title,b.content,b.created_at,b.user');
        $this->db->from('tbl_blog_tags a');
        $this->db->join('tbl_blog b', 'a.blog_id=b.blog_id', 'left');
        $this->db->join('tbl_tags c', 'a.tag_id=c.tag_id', 'left');
        $this->db->group_by('b.title');
        $this->db->order_by('b.created_at','desc');

        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    //get list blog by a tag
    public function getTagsBlogs($data) {
        $this->db->select('b.title,b.content,b.created_at,b.user,c.tag ');
        $this->db->from('tbl_blog_tags a');
        $this->db->join('tbl_blog b', 'a.blog_id=b.blog_id', 'left');
        $this->db->join('tbl_tags c', 'a.tag_id=c.tag_id', 'left');
        $this->db->where('c.tag', $data);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            return ($query->result_array());
        } else {
            return false;
        }
    }

    //create blog
    public function insert($data, $user) {
        $blog_id = $this->blog_insert($data,$user);
        if ($blog_id == -1) {
            return false;
        } else {
            return $this->tag_blog_insert($data['tag'], $blog_id);
        }
    }

    //insert into Blog Table
    //Insert into Blog, Check if tag isExist, insert into Tags, insert into Tag_Blog

    public function blog_insert($data,$user) {
        $this->db->set('title', $data['title']);
        $this->db->set('content', $data['content']);
        $this->db->set('user', $user);
        unset($data['tag']);
        if ($this->db->insert('tbl_blog', $data)) {
            return $insert_id = $this->db->insert_id();
        } else {
            return -1;
        }
    }

    //insert into table tag_blog
    public function tag_blog_insert($tags, $blog_id) {
        $tags = explode(',', strtolower($tags));
        $this->db->flush_cache();
        foreach ($tags as $key) {
            $id_tag = $this->tag_insert($key);
//            $this->db->set('blog_id', $blog_id);
//            $this->db->set('tag_id', $id_tag);
            $data = array(
                'blog_id' => $blog_id,
                'tag_id' => $id_tag
            );
            $this->db->insert('tbl_blog_tags', $data);
        }
        return true;
    }

    //insert into table tag
    public function tag_insert($key) {
        $id_key = $this->isTagExist($key);
        if ($id_key == -1) {
            $this->db->set('tag', $key);
            $this->db->insert('tbl_tags');
            return $insert_id = $this->db->insert_id();
        } else {
            return $id_key;
        }
    }


    //is tag exist?
    public function isTagExist($tag) {
        $this->db->where('tag', $tag);
        $query = $this->db->get('tbl_tags');
        $result = $query->result();
        if (!empty($result)) {
            return $result[0]->tag_id;
        } else {
            return -1;
        }
    }
}