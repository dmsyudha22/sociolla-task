<?php

class User extends CI_Model
{
    //get all User
    public function getAll() {
        $query = $this->db->get('tbl_user');
        return $query->result_array();
    }

    //insert user
    public function insert($data) {
        if ($this->isExist($data) > 0) {
            return false;
        } else {
            $this->db->insert('tbl_user', $data);
            return true;
        };
    }

    //is exist
    public function isExist($data) {
        $this->db->where('username', $data['username']);
        $query = $this->db->get('tbl_user');
        return $query->num_rows();
    }

}