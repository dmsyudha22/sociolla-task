<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/favicon.ico">

    <title>Sociolla Blog</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/css/blog.css" rel="stylesheet">
</head>

<body>

<header>
    <div class="blog-masthead">
        <div class="container">
            <nav class="nav">
                <a class="nav-link " href="<?= site_url('home') ?>">Home</a>
                <?= isset($_SESSION['session_log']) ? '<a class="nav-link" href=' . site_url('blogcontroller') . '>Create Blog</a>' : '' ?>
                <a class="nav-link active" href="#">Tags</a>
                <a class="nav-link" href="#">About</a>
            </nav>
        </div>
    </div>

    <div class="blog-header">
        <div class="container">
            <h1 class="blog-title">#Tags <?= $tagselected ?></h1>
            <p class="lead blog-description">Blog Post Tags</p>
        </div>
    </div>
</header>

<main role="main" class="container">
    <div class="row">
        <div class="col-md-12">
            <?php echo validation_errors('<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>', '</div>'); ?>

        </div>
    </div>
    <div class="row">

        <div class="col-sm-8 blog-main">

            <!-- /.blog-post -->
            <?php foreach ($result ? $result : array() as $row): ?>

                <div class="blog-post">
                    <h2 class="blog-post-title"><?php echo $row['title']; ?></h2>
                    <p class="blog-post-meta"><?php echo $row['created_at']; ?> by <a
                            href="#"><?php echo $row['user']; ?></a></p>
                    <p class="blog-post-meta"><?php $tagger = explode(',', $row['tag']);?>
                        <?php foreach ($tagger as $rows){?>
                            <a href="<?=site_url()?>/blogcontroller/getTags/<?=$rows?>"><?=$rows?></a>
                        <?php };?></p>
                    <p><?php echo $row['content'];?> </p>
                </div><!-- /.blog-post -->

            <?php endforeach; ?>
            <!-- /.blog-post -->

            <nav class="blog-pagination">
                <a class="btn btn-outline-primary" href="#">Older</a>
                <a class="btn btn-outline-secondary disabled" href="#">Newer</a>
            </nav>

        </div><!-- /.blog-main -->

        <aside class="col-sm-3 ml-sm-auto blog-sidebar">
            <div class="sidebar-module sidebar-module-inset">
                <h4>
                    Welcome <?= isset($_SESSION['session_log']) ? ', ' . ucwords($this->session->userdata['session_log']['username']) : ''; ?> </h4>
                <p><?= isset($_SESSION['session_log']) ? '<a href="' . site_url('home/logout') . '">Logout </a>' : '' ?></p>
            </div>
            <div class="sidebar-module">
                <?= !empty($message) ? '<div class="alert alert-danger alert-dismissible fade show">' . $message . '</div>' : '' ?>
                <?php echo form_open('home/login'); ?>
                <h4 class="form-signin-heading">Please Login</h4>
                <label for="username" class="sr-only">Username</label>
                <input type="text" id="username" name="username"
                       class="form-control" placeholder="Username/Email" autofocus>
                <br>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                </form>
            </div>

            <div class="sidebar-module">
                <?php echo form_open('home'); ?>
                <h4 class="form-signin-heading">or Register</h4>
                <label for="username" class="sr-only">Username</label>
                <input type="text" id="username" name="username" value="<?php echo set_value('username'); ?>"
                       class="form-control" placeholder="Username" autofocus>
                <br>
                <label for="email" class="sr-only">Email</label>
                <input type="email" id="email" name="email" value="<?php echo set_value('email'); ?>"
                       class="form-control" placeholder="Email">
                <br>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
                </form>
            </div>

            <div class="sidebar-module">
                <h4>Elsewhere</h4>
                <ol class="list-unstyled">
                    <li><a href="#">GitHub</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li><a href="#">Facebook</a></li>
                </ol>
            </div>
        </aside><!-- /.blog-sidebar -->

    </div><!-- /.row -->

</main><!-- /.container -->

<footer class="blog-footer">
    <p>Blog Sociolla copyright 2018<a href="https://dmsyudha.com">@dmsyudha</a>.
    </p>
    <p>
        <a href="#">Back to top</a>
    </p>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="<?=base_url()?>assets/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="<?= base_url() ?>assets/assets/js/vendor/popper.min.js"></script>
<script src="<?= base_url() ?>assets/dist/js/bootstrap.js"></script>
</body>
</html>