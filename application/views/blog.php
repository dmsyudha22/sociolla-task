<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url() ?>assets/favicon.ico">

    <title>Blog Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/css/blog.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/tagsinput.css" rel="stylesheet">
</head>

<body>

<header>
    <div class="blog-masthead">
        <div class="container">
            <nav class="nav">
                <a class="nav-link" href="<?=site_url('home')?>">Home</a>
                <a class="nav-link active" href="#">Create Post</a>
                <a class="nav-link" href="#">Tags</a>
                <a class="nav-link" href="#">About</a>
            </nav>
        </div>
    </div>

    <div class="blog-header">
        <div class="container">
            <h1 class="blog-title">Create Blog</h1>
            <p class="lead blog-description">Feel free to share with the world.</p>
        </div>
    </div>
</header>

<main role="main" class="container">

    <div class="row">
        <div class="col-md-12">
            <?=!empty($message)? '<div class="alert alert-danger alert-dismissible fade show">'.$message.'</div>':''?>
        </div>
        <div class="col-sm-8 blog-main">

            <?php echo form_open('blogcontroller/insert'); ?>
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title"  placeholder="Enter Title" maxlength="255">
                    <small id="titleHelp" class="form-text text-muted">Max 255 char.</small>
                </div>
                <div class="form-group">
                    <label for="tags">Tags</label>
                    <input type="text" name="tag" data-role="tagsinput">
                    <small  class="form-text text-muted">Enter for input more tags</small>
                </div>

                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea class="form-control" id="content" name="content" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        </div><!-- /.blog-main -->

        <aside class="col-sm-3 ml-sm-auto blog-sidebar">
            <div class="sidebar-module sidebar-module-inset">
                <h4>About</h4>
                <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div>
            <div class="sidebar-module">
                <h4>Archives</h4>
                <ol class="list-unstyled">
                    <li><a href="#">March 2014</a></li>
                    <li><a href="#">February 2014</a></li>
                    <li><a href="#">January 2014</a></li>
                    <li><a href="#">December 2013</a></li>
                    <li><a href="#">November 2013</a></li>
                    <li><a href="#">October 2013</a></li>
                    <li><a href="#">September 2013</a></li>
                    <li><a href="#">August 2013</a></li>
                    <li><a href="#">July 2013</a></li>
                    <li><a href="#">June 2013</a></li>
                    <li><a href="#">May 2013</a></li>
                    <li><a href="#">April 2013</a></li>
                </ol>
            </div>
            <div class="sidebar-module">
                <h4>Elsewhere</h4>
                <ol class="list-unstyled">
                    <li><a href="#">GitHub</a></li>
                    <li><a href="#">Twitter</a></li>
                    <li><a href="#">Facebook</a></li>
                </ol>
            </div>
        </aside><!-- /.blog-sidebar -->

    </div><!-- /.row -->

</main><!-- /.container -->

<footer class="blog-footer">
    <p>Blog Sociolla copyright 2018<a href="https://dmsyudha.com">@dmsyudha</a>.
    </p>
    <p>
        <a href="#">Back to top</a>
    </p>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="<?= base_url() ?>assets/assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="<?= base_url() ?>assets/assets/js/vendor/popper.min.js"></script>
<script src="<?= base_url() ?>assets/dist/js/bootstrap.js"></script>
<script src="<?= base_url() ?>assets/js/tagsinput.js"></script>
</body>
</html>
