<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct() {
        parent::__construct();
        $this->load->model('user');
        $this->load->library('form_validation');
        $this->load->model('blog');
    }

    //set session data
    public function set_session($data) {
        $session_data = array(
            'username' => $data['username']
        );
        $this->session->set_userdata('session_log', $session_data);
    }

    //load first time
    public function index() {
        $this->load->helper(array('form', 'url'));
        $result = $this->blog->getAll();
        $blog['result'] = $result;

        $this->form_validation->set_rules('username', 'Username', 'required|is_unique[tbl_user.username]');
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[tbl_user.email]');

        $data = $this->input->post();

        if ($this->form_validation->run() == FALSE) {
            if ($result) {
                $this->load->view('home', $blog);
            }else{
                $this->load->view('home',$blog);
            }
        } else {
            $blog['result'] = $result;
            $this->user->insert($data);
            $this->set_session($data);
            $this->load->view('home', $blog);
        }
    }

    //login control
    public function login() {
        $result = $this->blog->getAll();
        $blog['result'] = $result;
        $data = $this->input->post();
        if ($this->user->isExist($data) == 1) {
            $this->set_session($data);
            $this->load->view('home',$blog);
        } else {
            $blog['message'] = 'Username belum terdaftar';
            $this->load->view('home', $blog);
        }
    }

    //logout control

    public function logout() {
        $this->session->unset_userdata('session_log');
        redirect('home');
    }

}