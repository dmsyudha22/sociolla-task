<?php

class BlogController extends CI_Controller
{

    public function __construct() {
        parent::__construct();
        $this->load->model('user');
        $this->load->model('blog');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

    public function index() {
        $this->load->view('blog');
    }

    //insert blog
    public function insert() {
        $data = $this->input->post();
        $user = ucwords($this->session->userdata['session_log']['username']);
        $message = '';
        if (isset($_SESSION['session_log'])) {
            $this->blog->insert($data, $user);
            $message = 'Selamat, Anda berhasil input';
        } else {
            $message = 'Maaf, Anda belum login';
        };
        $this->load->view('blog', $message);
    }

    public function getTags($data){
        $result = $this->blog->getTagsBlogs($data);
        $blog['result'] = $result;
        $blog['tagselected'] = $data;
        $this->load->view('tag', $blog);
    }

}